#!/bin/bash

OUT=${OUT:-"/dockerdata-nfs/onap/tern"}

if [[ -z $TERN_INSTALL ]]; then
  TERN_LOCATION=${TERN_LOCATION:-"/home/debian"}
  cd $TERN_LOCATION
  python3 -m venv ternenv
  cd ternenv
  . bin/activate
  # current release has bug with image name parsing, need to install
  # from source until release (end of March 2021)
  git clone https://github.com/tern-tools/tern --branch main
  cd tern
  git checkout 551a4e41beaef6fc9a7d3ec286d6b98688d3a4f4

  python3 -m pip install wheel
  python3 -m pip install .
fi

sudo mkdir -p $OUT
sudo chown -R debian:debian $OUT

images=( $(kubectl get pods --namespace onap \
           -o jsonpath="{.items[*].spec.containers[*].image}" |\
           tr -s '[[:space:]]' '\n' | sort | uniq -u) )                                                              

for (( i=0; i<${#images[@]}; i++ ))
do
        if [[ ${images[$i]} == *"onap/vid"*  ]]; then
                echo "Analysis of ${images[$i]} ommited due to bug."
                continue
        fi
        echo "Analysis of ${images[$i]} started."
        report=$(echo  ${images[$i]} | tr '/' '_')
        tern report -f yaml -i ${images[$i]} 1> ${report}.yml 2> ${report}.log
        sudo mv ${report}* ${OUT}/
done
